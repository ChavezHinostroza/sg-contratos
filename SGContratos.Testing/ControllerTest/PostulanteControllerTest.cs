﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using Interfaces;
using Moq;
using NUnit.Framework;
using SGContratos;
using System.Web.Mvc;
using SGContratos.Controllers;
using Validators.PostulanteValidators;

namespace SGContratos.Testing.ControllerTest
{
    [TestFixture]
  public  class PostulanteControllerTest
    {
        [Test]
        public void RetornarVista()
        {
            var mock = new Mock<InterfacePostulante>();
            mock.Setup(o => o.All()).Returns(new List<Postulante>());

            var controller = new PostulanteController(mock.Object, null);
            var view = controller.Index();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Inicio", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Postulante>), view.Model);
        }
        [Test]
        public void LlamarMetodosDelRepository()
        {
            var mock = new Mock<InterfacePostulante>();
            mock.Setup(o => o.All()).Returns(new List<Postulante>());

            var controller = new PostulanteController(mock.Object, null);

            controller.Index();

            mock.Verify(o => o.All(), Times.Exactly(1));
        }

        [Test]
        public void TestCreateReturnView()
        {
            var controller = new PostulanteController(null, null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Create", view.ViewName);
        }

        [Test]
        public void TestPostCreateOKReturnRedirect()
        {
            var postulante = new Postulante();

            var repositoryMock = new Mock<InterfacePostulante>();

            repositoryMock.Setup(o => o.Store(new Postulante()));

            var validatorMock = new Mock<PostulanteValidator>();

            validatorMock.Setup(o => o.Pass(postulante)).Returns(true);

            var controller = new PostulanteController(repositoryMock.Object, validatorMock.Object);

            var redirect = (RedirectToRouteResult)controller.Create(postulante);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreateCallStoreMethodFromRepository()
        {
            var postulante = new Postulante();

            var repositoryMock = new Mock<InterfacePostulante>();

            var validatorMock = new Mock<PostulanteValidator>();

            validatorMock.Setup(o => o.Pass(postulante)).Returns(true);

            repositoryMock.Setup(o => o.Store(postulante));

            var controller = new PostulanteController(repositoryMock.Object, validatorMock.Object);

            var redirect = controller.Create(postulante);

            repositoryMock.Verify(o => o.Store(postulante), Times.Once());

        }

        [Test]

        public void TestPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var postulante = new Postulante { };

            var mock = new Mock<PostulanteValidator>();

            mock.Setup(o => o.Pass(postulante)).Returns(false);

            var controller = new PostulanteController(null, mock.Object);

            var view = controller.Create(postulante);

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }

    }
}
