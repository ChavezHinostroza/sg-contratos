﻿using Modelos;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validators.PostulanteValidators;

namespace SGContratos.Testing.Validators
{
    [TestFixture]
    public class PostulanteValidatorsTest
    {
        [Test]
        public void PostulanteSinNombreDebeRetornarFalse()
        {
            var validator = new PostulanteValidator();
            var postulante = new Postulante();

            var result = validator.Pass(postulante);

            Assert.False(result);
        }

        [Test]
        public void PostulanteConDatosDebeRetornarTrue()
        {
            var validator = new PostulanteValidator();
            var postulante = new Postulante()
            {
                Id = 3,
                ApellidoMaterno = "Vito",
                ApellidoPaterno = "Lovar",
                Celular = "976010998",
                CorreoElectronico = "ginnieEGGE123@hotmail.com",
                DNI = "98765439",
                Nombres= "gian",
                Sexo = "Femenino",
                Curriculum = "CurriGinnie"
            };

            var result = validator.Pass(postulante);
            Assert.True(result);
        }
    }
}
