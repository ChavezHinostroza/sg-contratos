﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Interfaces
{
   public interface IPreguntas
    {
        List<Preguntas> All();
        void Store(Preguntas preguntas);
        Preguntas Find(int Id);        
        void Update(Preguntas preguntas);
        void Delete(int Id);
        List<Preguntas> ByQueryAll(string query);
        Preguntas GenerarPregAleatorias(Int32 Id, String puesto);
    }
}
