﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using DB.SGContratos;

namespace GenerateDb
{
    class Program
    {
        static void Main(string[] args)
        {
            var Postulante01 = new Postulante()
            {
                Nombres = "Carlos",
                ApellidoPaterno = "Chavrz",
                ApellidoMaterno = "Hinostroza",
                Sexo = "Masculino",
                Celular = "9763462566",
                CorreoElectronico = "carlo@hotmail.com",
                DNI = "78451976",
                Curriculum = "Si"
            };
            var Postulante02 = new Postulante()
            {
                Nombres = "Tony",
                ApellidoPaterno = "Torres",
                ApellidoMaterno = "Vargas",
                Sexo = "Femenino",
                Celular = "9784512324",
                CorreoElectronico = "Hijomipona@jajaj.com",
                DNI = "78412563",
                Curriculum = "Si"
            };
            var Postulante03 = new Postulante()
            {
                Nombres = "Yisus",
                ApellidoPaterno = "Chegne",
                ApellidoMaterno = "Chavez",
                Sexo = "Masculino",
                Celular = "9764518235",
                CorreoElectronico = "chegene@yahhoo.es",
                DNI = "12457896",
                Curriculum = "Si"
            };
            var pregunta01 = new Preguntas()
            {
                Pregunta = "Qué hacer en caso de perder información",
                Puesto = "Puesto 1",
            };
            var pregunta02 = new Preguntas()
            {
                Pregunta = "¿Cómo crear una base de datos?",
                Puesto = "Puesto 1"
            };
            var pregunta03 = new Preguntas()
            {
                Pregunta = "¿Qué es una arquitectura en capas?",
                Puesto = "Puesto 1"
            };
            var pregunta04 = new Preguntas()
            {
                Pregunta = "¿Qué culpa tiene Fatmagul?",
                Puesto = "Puesto 1"
            };
            var pregunta05 = new Preguntas()
            {
                Pregunta = "¿Qué es un Moq y cómo se usa?",
                Puesto = "Puesto 1"
            };
            var pregunta06 = new Preguntas()
            {
                Pregunta = "Una razón para crear un procedimiento almacenado es: ",
                Puesto = "Puesto 1"
            };
            var pregunta07 = new Preguntas()
            {
                Pregunta = "¿Cuales son los lenguajes que más domina?",
                Puesto = "Puesto 1"
            };
            var pregunta08 = new Preguntas()
            {
                Pregunta = "Si dos y dos son cuatro, cuatro y dos son seis, ¿Cuánto es seis y dos?",
                Puesto = "Puesto 1"
            };
            var pregunta09 = new Preguntas()
            {
                Pregunta = "Programe una calculadora. Utilice el lenguaje que desee",
                Puesto = "Puesto 1"
            };
            var pregunta10 = new Preguntas()
            {
                Pregunta = "Usted ejecuta la instrucción: INCERT INTO Carretera VALUES (1234, 36) Cual es el resultado? ",
                Puesto = "Puesto 1"
            };
            var pregunta11 = new Preguntas()
            {
                Pregunta = "¿Cuáles son los pilares de acreditación?",
                Puesto = "Puesto 1"
            };
            var pregunta12 = new Preguntas()
            {
                Pregunta = "¿Nivel de Inglés?",
                Puesto = "Puesto 1"
            };
            var pregunta13 = new Preguntas()
            {
                Pregunta = "¿Qué es SEDIPRO?",
                Puesto = "Puesto 1"
            };
            var pregunta14 = new Preguntas()
            {
                Pregunta = "Mencione algunos ejemplos sobre STREAMS ORIENTADOS A BYTE",
                Puesto = "Puesto 1"
            };
            var pregunta15 = new Preguntas()
            {
                Pregunta = "¿Por qué decidió estudiar esta carrera?",
                Puesto = "Puesto 1"
            };
            var pregunta16 = new Preguntas()
            {
                Pregunta = "¿Cómo trabaja code First?",
                Puesto = "Puesto 1"
            };
            var pregunta17 = new Preguntas()
            {
                Pregunta = "¿La arquitectura de capas puede tener varias capas?",
                Puesto = "Puesto 1"
            };
            var pregunta18 = new Preguntas()
            {
                Pregunta = "Mostrar los ids de las categorías de productos utilizando solo la información de la tabla producto.",
                Puesto = "Puesto 1"
            };
            var pregunta19 = new Preguntas()
            {
                Pregunta = "Crear una consulta para mostrar el nombre de producto y stock para las productos que tienen un stock mayor que 50",
                Puesto = "Puesto 1"
            };
            var pregunta20 = new Preguntas()
            {
                Pregunta = "Crear una consulta para mostrar el  apellido paterno y nombres para  la persona con R.U.C igual a -10266115645-",
                Puesto = "Puesto 1"
            };
            var pregunta21 = new Preguntas()
            {
                Pregunta = "Mostrar todos los datos de las ventas que se hicieron en el mes mayo ordenados por fecha de venta en orden ascendente.",
                Puesto = "Puesto 1"
            };
            var pregunta22 = new Preguntas()
            {
                Pregunta = "Actualizar la información de la tabla persona de modo que todos los empleados que tienen como apellido -Flores- estén ubicados en Lima",
                Puesto = "Puesto 1"
            };
            var pregunta23 = new Preguntas()
            {
                Pregunta = "Escribir una sentencia SQL que elimine los productos con stock igual a 100 de la categoría -Acabados-",
                Puesto = "Puesto 1"
            };
            var pregunta24 = new Preguntas()
            {
                Pregunta = "Modifique el stock de los productos que empiezan con la palabra ‘Adaptador’ a un valor de 50.",
                Puesto = "Puesto 1"
            };
            var pregunta25 = new Preguntas()
            {
                Pregunta = "Escribir una sentencia SQL que aumente el precio de los productos con id igual a ‘001’ en un 20%",
                Puesto = "Puesto 1"
            };
            var pregunta26 = new Preguntas()
            {
                Pregunta = "Eliminar la columna que guarda información de la marca del equipo.",
                Puesto = "Puesto 1"
            };
            var pregunta27 = new Preguntas()
            {
                Pregunta = "Cambiar el nombre de la columna estado a estado_equipo",
                Puesto = "Puesto 1"
            };
            var pregunta28 = new Preguntas()
            {
                Pregunta = "¿Qué tipo de índice cambia el orden en el que los datos se almacenan en una tabla ?",
                Puesto = "Puesto 1"
            };
            var pregunta29 = new Preguntas()
            {
                Pregunta = "¿Los términos mapas de bits árbol B y hash a que tipo de estructura de base de datos se refiere...?",
                Puesto = "Puesto 1"
            };
            var pregunta30 = new Preguntas()
            {
                Pregunta = "¿Qué define la cantidad de espacio de almacenamiento asignado a un valor columna?",
                Puesto = "Puesto 1"
            };


            var _context = new SGContratosContext();
            Console.WriteLine("Creando Base De Datos");

            _context.Postulantes.Add(Postulante01);
            _context.Postulantes.Add(Postulante02);
            _context.Postulantes.Add(Postulante03);
            _context.Preguntas.Add(pregunta01);
            _context.Preguntas.Add(pregunta02);
            _context.Preguntas.Add(pregunta03);
            _context.Preguntas.Add(pregunta04);
            _context.Preguntas.Add(pregunta05);
            _context.Preguntas.Add(pregunta06);
            _context.Preguntas.Add(pregunta07);
            _context.Preguntas.Add(pregunta08);
            _context.Preguntas.Add(pregunta09);
            _context.Preguntas.Add(pregunta10);
            _context.Preguntas.Add(pregunta11);
            _context.Preguntas.Add(pregunta12);
            _context.Preguntas.Add(pregunta13);
            _context.Preguntas.Add(pregunta14);
            _context.Preguntas.Add(pregunta15);
            _context.Preguntas.Add(pregunta16);
            _context.Preguntas.Add(pregunta17);
            _context.Preguntas.Add(pregunta18);
            _context.Preguntas.Add(pregunta19);
            _context.Preguntas.Add(pregunta20);
            _context.Preguntas.Add(pregunta21);
            _context.Preguntas.Add(pregunta22);
            _context.Preguntas.Add(pregunta23);
            _context.Preguntas.Add(pregunta24);
            _context.Preguntas.Add(pregunta25);
            _context.Preguntas.Add(pregunta26);
            _context.Preguntas.Add(pregunta27);
            _context.Preguntas.Add(pregunta28);
            _context.Preguntas.Add(pregunta29);
            _context.Preguntas.Add(pregunta30);

            _context.SaveChanges();

            Console.WriteLine("Base de datos creada con exito!!");
            Console.ReadLine();
        }
    }
}
