﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Validators.PreguntasValidators
{
  public class PreguntasValidators
    {
      public virtual bool Pass(Preguntas preguntas)
      {
          if (String.IsNullOrEmpty(preguntas.Pregunta))
              return false;
          if (String.IsNullOrEmpty(preguntas.Puesto))
              return false;
         
          return true;
      }
    }
}
