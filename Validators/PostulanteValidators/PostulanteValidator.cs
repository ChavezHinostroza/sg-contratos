﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validators.PostulanteValidators
{
    public class PostulanteValidator
    {
        public virtual bool Pass(Postulante postulante)
        {
            if (String.IsNullOrEmpty(postulante.ApellidoPaterno))
                return false;
            if (String.IsNullOrEmpty(postulante.DNI))
                return false;
            if (String.IsNullOrEmpty(postulante.Sexo))
                return false;
            if (String.IsNullOrEmpty(postulante.ApellidoMaterno))
                return false;
            if (String.IsNullOrEmpty(postulante.Nombres))
                return false;
            if (String.IsNullOrEmpty(postulante.Celular))
                return false;
            if (String.IsNullOrEmpty(postulante.CorreoElectronico))
                return false;
            return true;
        }
    }
}
