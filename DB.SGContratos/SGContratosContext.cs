﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB.SGContratos.Mapping;
using System.Data.Entity;
using Modelos;

namespace DB.SGContratos
{
    public class SGContratosContext : DbContext
    {
        public SGContratosContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SGContratosContext>());
        }
        public DbSet<Postulante> Postulantes { get; set; }
        public DbSet<Preguntas> Preguntas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PostulanteMap());
            modelBuilder.Configurations.Add(new PreguntasMap());
        }
    }
}
