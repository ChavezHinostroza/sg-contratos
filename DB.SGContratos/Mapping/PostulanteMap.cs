﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Modelos;

namespace DB.SGContratos.Mapping
{
    public class PostulanteMap : EntityTypeConfiguration<Postulante>
    {
        public PostulanteMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.ApellidoMaterno).IsRequired().HasMaxLength(50);
            this.Property(p => p.Nombres).IsRequired().HasMaxLength(50);
            this.Property(p => p.DNI).IsRequired();
            this.Property(p => p.ApellidoPaterno).IsRequired().HasMaxLength(50);
            this.Property(p => p.Celular).IsRequired();
            this.Property(p => p.CorreoElectronico).IsRequired().HasMaxLength(50);
            this.Property(p => p.Sexo).IsRequired();
      
            this.ToTable("Postulante");
        }
    }
}
