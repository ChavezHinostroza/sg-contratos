﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Modelos;

namespace DB.SGContratos.Mapping
{
   public class PreguntasMap:EntityTypeConfiguration<Preguntas>
    {
        public PreguntasMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Pregunta).IsRequired().HasMaxLength(250);
            this.Property(p => p.Puesto).IsRequired().HasMaxLength(50);
           
      
            this.ToTable("PreguntasConocimiento");
        }
    }
}
