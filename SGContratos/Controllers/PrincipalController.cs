﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Interfaces;
using Modelos;
using Validators.PostulanteValidators;

namespace SGContratos.Controllers
{
    public class PrincipalController : Controller
    {
        //
        // GET: /Home/
        [HttpGet]
        public ViewResult Index()
        {
            return View("MenuPrincipal");
        }

    }
}
