﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelos;
using Interfaces;
using Validators.PreguntasValidators;

namespace SGContratos.Controllers
{
    public class PreguntasController : Controller
    {
        //
        // GET: /Preguntas/
        private IPreguntas repository;
        private PreguntasValidators validator;
        private List<Preguntas> preguntasTemp;
        private Random r;

        public PreguntasController(IPreguntas repository, PreguntasValidators validator)
        {
            this.repository = repository;
            this.validator = validator;
            preguntasTemp = new List<Preguntas>();
            r = new Random(DateTime.Now.Millisecond);
        }

        [HttpGet]
        public ActionResult Prueba(int? cantidad, string puesto)
        {
            //FALTA
            //No se repitan las preguntas
            int[] aleatorios = new int[50];
            Preguntas datos;
            if (cantidad == null)
                cantidad = 0;
            for (int i = 0; i < cantidad; i++)
            {
                aleatorios[i] = r.Next(1, 30);
                if (i > 0)    // a partir del segundo numero que genera empezara a comparar que no se repita
                    for (int x = 0; x < 30; x++)  //comprobara que no se repita por 30 veces
                        for (int j = 0; j < i; j++)
                            if (aleatorios[i] == aleatorios[j])
                                aleatorios[i] = r.Next(1, 30);
            }
            //Mostrar preguntas
            for (int i = 0; i < cantidad; i++)
            {
                datos = repository.GenerarPregAleatorias(aleatorios[i],puesto);
                preguntasTemp.Add(datos);
            }
            return View("Prueba", preguntasTemp);
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("Inicio", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(Preguntas preguntas)
        {

            if (validator.Pass(preguntas))
            {
                repository.Store(preguntas);

                TempData["UpdateSuccess"] = "Se Guardó Correctamente";

                return RedirectToAction("Index");
            }

            return View("Inicio", preguntas);
        }

        [HttpGet]
        public ViewResult Edit(int Id)
        {
            var data = repository.Find(Id);
            return View("Editar", data);
        }

        [HttpPost]
        public ActionResult Edit(Preguntas preguntas)
        {
            repository.Update(preguntas);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }
    }
}
